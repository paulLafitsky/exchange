//
//  Transaction.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/10/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

struct Transaction {
    var from: Currency
    var to: Currency
    var amount: Double
}
