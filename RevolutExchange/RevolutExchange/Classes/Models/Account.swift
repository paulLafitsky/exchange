//
//  Account.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/8/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

class Account {
    var unit: Double
    var currency: Currency
    
    init(_ unit: Double, currency: Currency) {
        self.unit = unit
        self.currency = currency
    }
}
