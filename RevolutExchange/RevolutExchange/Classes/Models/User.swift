//
//  User.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/10/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

typealias Assets = [Account]

struct User {
    let assets: Assets
}


