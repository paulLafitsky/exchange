//
//  Currency.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/3/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation
import SWXMLHash

enum CurrencyParsingError: Error {
    case currencyAtributeNotFound
}

struct CurrencyRate: XMLIndexerDeserializable {
    let code: String
    let value: String
    
    static func deserialize(_ node: XMLIndexer) throws -> CurrencyRate {
        guard let code = node.element?.attribute(by: "currency") else {
            throw CurrencyParsingError.currencyAtributeNotFound
        }
        guard let price = node.element?.attribute(by: "rate") else {
            throw CurrencyParsingError.currencyAtributeNotFound
        }
        return CurrencyRate(code: code.text, value: price.text)
    }
}

extension CurrencyRate: Equatable {}

func == (a: CurrencyRate, b: CurrencyRate) -> Bool {
    return a.code == b.code && a.value == b.value
}
