//
//  Currency.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/8/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

enum Currency: String {
    case usd = "USD"
    case gbp = "GBP"
    case eur = "EUR"
    
    var name: String {
        switch self {
        case .usd:
            return "United States Dollar"
        case .gbp:
            return "British Pound"
        case .eur:
            return "Euro"
        }
    }
    
    var symbol: String {
        switch self {
        case .usd:
            return "$"
        case .gbp:
            return "£"
        case .eur:
            return "€"
        }
    }
}
