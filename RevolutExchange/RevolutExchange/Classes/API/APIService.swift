//
//  APIService.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/1/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Moya
import Alamofire

typealias SuccessCallback =  (_ result: Response) -> Void
typealias FailureCallback = (_ fail: Error) -> Void

protocol APIService {
    var apiProvider: MoyaProvider <APIRouter> { get }
    func request(router: APIRouter, succes: @escaping SuccessCallback, failure: @escaping FailureCallback)
}

class APIServiceImplementation: APIService {
    var apiProvider: MoyaProvider <APIRouter>
    
    init(apiProvider: MoyaProvider <APIRouter>) {
        self.apiProvider = apiProvider
    }
    
    func request(router: APIRouter, succes: @escaping SuccessCallback, failure: @escaping FailureCallback) {
        apiProvider.request(router) { (result) in
            switch result {
            case .success(let response):
                succes(response)
                break
            case .failure(let error):
                failure(error)
                break
            }
        }
    }
}
