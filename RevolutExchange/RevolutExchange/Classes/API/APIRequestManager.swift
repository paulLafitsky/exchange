//
//  APIRequestManager.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/1/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation
import Moya
import SWXMLHash

typealias CompleationBlock = () -> Void
typealias CurrencyCompleationBlock = ([CurrencyRate]) -> Void
typealias ErrorCompleationBlock = (Error) -> Void

protocol APIRequestManager {
    var apiService: APIService { get }
    func getExchangesCurrenciesRate(succes: @escaping CurrencyCompleationBlock, failure: @escaping ErrorCompleationBlock)
}

class APIRequestManagerImpl: APIRequestManager {
    var apiService: APIService
    
    init(apiService: APIService) {
        self.apiService = apiService
    }
    
    func getExchangesCurrenciesRate(succes: @escaping CurrencyCompleationBlock, failure: @escaping ErrorCompleationBlock) {
        apiService.request(router: APIRouter.EuroForeignExchange, succes: { (response) in
            let xml = SWXMLHash.parse(response.data)
            do {
                let currencyList:[CurrencyRate] = try xml["gesmes:Envelope"]["Cube"]["Cube"]["Cube"].value()
                succes(currencyList)
            }
            catch {
                failure(error)
            }
        }) { (error) in
            failure(error)
        }
    }
}
