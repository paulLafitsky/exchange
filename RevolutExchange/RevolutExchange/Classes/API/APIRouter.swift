//
//  ExchangeAPIRouter.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/1/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation
import Moya

// MARK:- Enum Declaration

enum APIRouter {
    case EuroForeignExchange
}

// MARK:- Moya Target

extension APIRouter: TargetType {
    
    var baseURL: URL {
        switch self {
        case .EuroForeignExchange:
            return URL(string: "http://www.ecb.europa.eu")!
        }
    }
    
    var path: String {
        switch self {
        case .EuroForeignExchange:
            return "/stats/eurofxref/eurofxref-daily.xml"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .EuroForeignExchange:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .EuroForeignExchange:
            return stubbedResponse(filename: "")
        }
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return [:]
    }
}

// MARK:- Stubbed Response

//TODO:- Add stubbed response

extension APIRouter: StubAPIRouter {
    
    func stubbedResponse(filename: String) -> Data {
        class TestClass { }
        let bundle = Bundle(for: TestClass.self)
        let path = bundle.path(forResource: filename, ofType: "xml")!
        return try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
    }
    
    func emptyResponse() -> Data {
        return "".data(using: .utf8)!
    }
}
