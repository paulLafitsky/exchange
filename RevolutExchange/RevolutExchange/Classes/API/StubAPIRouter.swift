//
//  StubAPIRouter.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/1/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

protocol StubAPIRouter {
    func stubbedResponse(filename: String) -> Data
    func emptyResponse() -> Data
}


