//
//  String+Validation.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/17/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

extension String {
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {
        if (self.isEmpty) {
            return true
        }
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        let decimalSeparator = formatter.decimalSeparator ?? "."
        
        if formatter.number(from: self) != nil {
            let split = self.components(separatedBy: decimalSeparator)
            let digits = split.count == 2 ? split.last ?? "" : ""
            return digits.count <= maxDecimalPlaces
        }
        return false
    }
}
