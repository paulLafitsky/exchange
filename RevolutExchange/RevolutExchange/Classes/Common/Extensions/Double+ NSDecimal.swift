//
//  Double+ NSDecimal.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/24/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

extension Double {
    func toNSDecimal() -> NSDecimalNumber{
        return NSDecimalNumber(value: self)
    }
}
