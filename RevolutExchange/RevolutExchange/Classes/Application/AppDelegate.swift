//
//  AppDelegate.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 9/30/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
