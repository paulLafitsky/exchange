//
//  CurrencyManager.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/8/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

class CurrencyManager {
    
    func getRateForTransaction(fromCurrency: Currency, toCurrency: Currency , rates: [CurrencyRate]) -> Double {
        if (fromCurrency == .eur) {
            return findRateForCurrency(currency: toCurrency, rates: rates)
        }
        let fromRate = findRateForCurrency(currency: toCurrency, rates: rates)
        let toRate = findRateForCurrency(currency: toCurrency, rates: rates)
        return toRate / fromRate
    }
    
    private func findRateForCurrency(currency: Currency, rates: [CurrencyRate]) -> Double{
        guard let rate = rates.first(where: {$0.code == currency.rawValue}), let rateValue = Double(rate.value) else { return 0 }
        return rateValue
    }
}
