//
//  UserManager.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/9/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

class UserManager {
    let user: User
    static let decimalNumberBehaviour = NSDecimalNumberHandler(roundingMode: .bankers,
                                                               scale: 2,
                                                               raiseOnExactness: false,
                                                               raiseOnOverflow: false,
                                                               raiseOnUnderflow: false,
                                                               raiseOnDivideByZero: false)
    
    static let defaultUserManager = UserManager(user: User(assets: [Account (100, currency: .eur),
                                                             Account (100, currency: .usd),
                                                             Account (100, currency: .gbp)]))
    
    private init(user: User) {
        self.user = user
    }
    
    func estimate(transaction: Transaction, rate: Double) -> Double {
        let estimation = NSDecimalNumber(value: transaction.amount).multiplying(by: NSDecimalNumber(value: rate), withBehavior: UserManager.decimalNumberBehaviour)
        return estimation.doubleValue        
    }
    
    func exchange(transaction: Transaction, rate: Double) -> Bool {
        guard let fromIndex = user.assets.index(where: {$0.currency == transaction.from}) else {return false}
        guard let toIndex = user.assets.index(where: {$0.currency == transaction.to}) else {return false}
        
        let toAmount = transaction.amount.toNSDecimal().multiplying(by: rate.toNSDecimal(), withBehavior: UserManager.decimalNumberBehaviour)
        let fromAmount = transaction.amount.toNSDecimal()
        
        let toExpectedAmount = user.assets[toIndex].unit.toNSDecimal().adding(toAmount).doubleValue
        let fromExpectedAmount = user.assets[fromIndex].unit.toNSDecimal().subtracting(fromAmount).doubleValue
        if (fromExpectedAmount < 0) {
            return false
        }
        user.assets[toIndex].unit = toExpectedAmount
        user.assets[fromIndex].unit = fromExpectedAmount
        return true
    }
}
