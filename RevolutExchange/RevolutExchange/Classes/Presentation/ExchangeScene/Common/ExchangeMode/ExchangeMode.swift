//
//  ExchangeMode.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/7/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

enum ExchangeMode {
    case from
    case to
}
