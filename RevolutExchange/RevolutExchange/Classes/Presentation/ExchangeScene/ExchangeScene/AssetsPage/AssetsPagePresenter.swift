//
//  ConcreteCurrencyPagePresenter.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/8/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation
import UIKit

fileprivate  struct AssetsPageConstant {
    static let storyboardName = "Main"
    static let controllerIdentifier = "ConcreteCurrencyViewControllerID"
}

// I want to separate interface, but can't come up with good name.

protocol AssetsPagePresenterPublic: class {
    var model: AssetsPageModel { get }
    var view: AssetsPageViewController? {get}
    func isActive() -> Bool
    func getActiveCurrency() -> Currency
    func showFutureBalance(currency: Currency, balance: Double)
    func reset()
}

protocol AssetsPagePresenter {
    func initialViewControlelr() -> ConcreteCurrencyViewController
    func nextViewController(before viewController: ConcreteCurrencyViewController) -> ConcreteCurrencyViewController
    func nextViewController(after viewController: ConcreteCurrencyViewController) -> ConcreteCurrencyViewController
    func presentationCount(for pageViewController: UIPageViewController) -> Int
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    func pageViewControllerDidFinishAnimation(pageViewController: UIPageViewController)
}

protocol AssetsPageDelegate: class {
    func assetsDidTriggeredAmountChanged(amount: Double, currency: Currency, mode: ExchangeMode)
    func assetsDidActiveCurrencyChanged(currency: Currency, mode: ExchangeMode)
}

//MARK: - Impl

class AssetsPagePresenterImpl: AssetsPagePresenterPublic,AssetsPagePresenter, ConcreteCurrencyPresenterDelegate {
    var model: AssetsPageModel
    weak var view: AssetsPageViewController?
    weak var delegate: AssetsPageDelegate?
    private var controllers = [ConcreteCurrencyViewController]()
    private var presenters = [ConcreteCurrencyPresenter]()
    private var activePresenter: ConcreteCurrencyPresenter?
    private var selectedIndex = 0
    
    init(model : AssetsPageModel, view: AssetsPageViewController) {
        self.model = model
        self.view = view
        configureDataSource()
    }
    
    //MARK: - AssetsPagePresenterPublic
    
    func showFutureBalance(currency: Currency, balance: Double) {
        let currencyPresenter = presenters.first(where: {$0.model.currency == currency})
        currencyPresenter?.updateFutureBalance(balance: balance.description)
    }
    
    func reset() {
        self.presenters[selectedIndex].reset()
    }
    
    func getActiveCurrency() -> Currency {
        return model.assets[selectedIndex].currency
    }
    
    func isActive() -> Bool {
        return activePresenter != nil
    }
    
    //MARK: - AssetsPagePresenter

    func configureDataSource() {
        for account in model.assets {
            let storyboard = UIStoryboard(name: AssetsPageConstant.storyboardName, bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: AssetsPageConstant.controllerIdentifier)
                as? ConcreteCurrencyViewController else { return }
            let model = ConcreteCurrencyModelImpl(currency: account.currency)
            let presenter = ConcreteCurrencyPresenterImpl(model: model, view: vc)
            presenter.delegate = self
            vc.presenter = presenter
            self.presenters.append(presenter)
            self.controllers.append(vc)
        }
    }
    
    func initialViewControlelr() -> ConcreteCurrencyViewController {
        return controllers.first!
    }
    
    func nextViewController(before viewController: ConcreteCurrencyViewController) -> ConcreteCurrencyViewController {
        guard var index = controllers.index(of: viewController) else {
            return ConcreteCurrencyViewController()
        }
        if (index == controllers.count - 1) {
            index = 0
            return controllers[index]
        }
        index =  index + 1
        return controllers[index]
    }
    
    func nextViewController(after viewController: ConcreteCurrencyViewController) -> ConcreteCurrencyViewController {
        guard var index = self.controllers.index(of: viewController) else {
            return ConcreteCurrencyViewController()
        }
        if (index == 0) {
            index = controllers.count - 1
            return controllers[index]
        }
        index =  index - 1
        return controllers[index]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return model.assets.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        guard let presentedVc = pageViewController.viewControllers?.first,
              let currencyVC = presentedVc as? ConcreteCurrencyViewController,
              let index = self.controllers.index(of: currencyVC) else {
            return 0
        }
        return index
    }
    
    func pageViewControllerDidFinishAnimation(pageViewController: UIPageViewController) {
        selectedIndex = presentationIndexForPageViewController(pageViewController: pageViewController)
        delegate?.assetsDidActiveCurrencyChanged(currency: model.assets[selectedIndex].currency, mode: model.mode)
    }
    
    //MARK: - ConcreteCurrencyPresenterDelegate
    
    func currencyPresenterDidAmountChange(amount: Double, presenter: ConcreteCurrencyPresenter) {
        delegate?.assetsDidTriggeredAmountChanged(amount: amount, currency: presenter.model.currency, mode: model.mode)
    }
    
    func currencyPresenterDidBecameActive(presenter: ConcreteCurrencyPresenter) {
        activePresenter = presenter
    }
    
    func currencyPresenterResignActive(presenter: ConcreteCurrencyPresenter) {
        if (activePresenter?.model.currency == presenter.model.currency) {
            activePresenter = nil
        }
    }
}
