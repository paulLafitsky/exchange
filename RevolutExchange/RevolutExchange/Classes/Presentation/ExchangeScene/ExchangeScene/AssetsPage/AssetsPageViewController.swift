//
//  PageSceneViewController.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/6/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

class AssetsPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var presenter: AssetsPagePresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        setViewControllers([presenter.initialViewControlelr()], direction: .forward, animated: true, completion: nil)
    }
        
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return presenter.nextViewController(before: viewController as! ConcreteCurrencyViewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return presenter.nextViewController(after: viewController as! ConcreteCurrencyViewController)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return presenter.presentationIndexForPageViewController(pageViewController: pageViewController)
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return presenter.presentationIndexForPageViewController(pageViewController:pageViewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (!completed) {
            return
        }
        presenter.pageViewControllerDidFinishAnimation(pageViewController: pageViewController)
    }
}

