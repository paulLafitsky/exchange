//
//  ConcreteCurrencyPageModel.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/8/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation

protocol AssetsPageModel {
    var assets: Assets { get }
    var mode: ExchangeMode { get }
}

class AssetsPageModelImpl: AssetsPageModel {
    var assets: Assets
    var mode: ExchangeMode
    
    init(assets: Assets, mode: ExchangeMode) {
        self.mode = mode
        switch mode {
        case .to:
            self.assets = assets.reversed()
            break
        default:
            self.assets = assets
        }
    }
}

