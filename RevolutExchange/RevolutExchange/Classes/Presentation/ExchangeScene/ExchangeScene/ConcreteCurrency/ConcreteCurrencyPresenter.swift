//
//  ConcreteCurrencyPresenter.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/7/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

// The same naming problem here

protocol ConcreteCurrencyPresenter {
    var model: ConcreteCurrencyModel { get }
    var view: ConcreteCurrencyViewController? { get }
    func updateFutureBalance(balance: String)
    func reset()
}

protocol CurrencyPresenter {
    func didReady()
    func shouldChangeCharactersIn(_ textField: UITextField, range: NSRange, string: String) -> Bool
    func didBecameFirstResponder()
    func didResignFirstResponder()
}

protocol ConcreteCurrencyPresenterDelegate: class {
    func currencyPresenterDidAmountChange(amount: Double, presenter: ConcreteCurrencyPresenter)
    func currencyPresenterDidBecameActive(presenter: ConcreteCurrencyPresenter)
    func currencyPresenterResignActive(presenter: ConcreteCurrencyPresenter)
}

class ConcreteCurrencyPresenterImpl: ConcreteCurrencyPresenter, CurrencyPresenter {
    var model: ConcreteCurrencyModel
    weak var view: ConcreteCurrencyViewController?
    weak var delegate: ConcreteCurrencyPresenterDelegate?
    
    init(model: ConcreteCurrencyModel , view: ConcreteCurrencyViewController) {
        self.model = model
        self.view = view
    }
    
    func updateFutureBalance(balance: String) {
        view?.updateFeatureBalanceForTextField(willReceiveValue: balance)
    }
    
    func reset() {
        self.didReady()
    }
    
    //MARK:- CurrencyPresenter
    
    func shouldChangeCharactersIn(_ textField: UITextField, range: NSRange, string: String) -> Bool {
        guard let currentText = textField.text else {return false}
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
        if replacementText.isEmpty {
            view?.updateBalanceState(isEnough: model.isValidAmount(amount: replacementText))
            delegate?.currencyPresenterDidAmountChange(amount: 0, presenter: self)
            return true
        }
        if (isStringContainChar(string: replacementText)) {
            return false
        }
        let isValid = replacementText.isValidDouble(maxDecimalPlaces: 2)
        if isValid {
            guard let amount = Double(replacementText) else {return false }
            delegate?.currencyPresenterDidAmountChange(amount: amount, presenter: self)
        }
        view?.updateBalanceState(isEnough: model.isValidAmount(amount: replacementText))
        return isValid
    }
    
    func didReady() {
        view?.configureInitialState(model.balanceDescription(), symbol: model.currency.symbol)
    }
    
    func didBecameFirstResponder() {
        delegate?.currencyPresenterDidBecameActive(presenter: self)
    }
    
    func didResignFirstResponder() {
        delegate?.currencyPresenterResignActive(presenter: self)
    }
    
    func isStringContainChar(string: String) -> Bool {
        let charSet = CharacterSet.letters
        guard let _ = string.rangeOfCharacter(from: charSet) else {return false}
        return true
    }
}
