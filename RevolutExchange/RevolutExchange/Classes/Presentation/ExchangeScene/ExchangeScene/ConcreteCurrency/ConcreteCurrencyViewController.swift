	//
//  ConcreteCurrencyViewController.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/7/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

class ConcreteCurrencyViewController: UIViewController, UITextFieldDelegate{
    var presenter: CurrencyPresenter!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var inputTextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.didReady()
        inputTextfield.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return presenter.shouldChangeCharactersIn(textField, range: range, string: string)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        presenter.didBecameFirstResponder()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        presenter.didResignFirstResponder()
        return true
    }
    
    func configureInitialState(_ balance: String, symbol: String) {
        inputTextfield.placeholder = "0"
        inputTextfield.text = ""
        balanceLabel.text = balance
        symbolLabel.text = symbol
    }
    
    func updateBalance(balance: String) {
        inputTextfield.text = balance
    }
    
    func updateFeatureBalanceForTextField(willReceiveValue: String) {
        inputTextfield.text = willReceiveValue
    }
    
    func updateBalanceState(isEnough: Bool) {
        balanceLabel.textColor = isEnough ? .black : .red
    }
}
