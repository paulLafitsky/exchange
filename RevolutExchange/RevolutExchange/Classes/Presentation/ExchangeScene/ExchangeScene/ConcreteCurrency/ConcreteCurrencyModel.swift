//
//  ConcreteCurrencyModel.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/7/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

protocol ConcreteCurrencyModel {
    var currency: Currency { get }
    func isValidAmount(amount: String) -> Bool
    func balanceDescription() -> String
}

class ConcreteCurrencyModelImpl: ConcreteCurrencyModel {
    var currency: Currency

    init(currency: Currency) {
        self.currency = currency
    }
    
    func balanceDescription() -> String {
        guard let account = UserManager.defaultUserManager.user.assets.first(where: {$0.currency == currency}) else {return ""}
        return "You have \(currency.symbol) \(account.unit)"
    }
    
    func isValidAmount(amount: String) -> Bool {
        if amount.isEmpty { return true }
        guard let account = UserManager.defaultUserManager.user.assets.first(where: {$0.currency == currency}) else {return false}
        guard let doubleAmount = Double(amount) else {return false}
        return doubleAmount <= account.unit
    }
}
