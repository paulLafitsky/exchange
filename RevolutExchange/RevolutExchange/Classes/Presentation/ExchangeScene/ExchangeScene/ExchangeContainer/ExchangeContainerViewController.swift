//
//  ExchangeSceneViewController.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/5/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

class ExchangeContainerViewController: UIViewController {
    private var configurator = ExchangeContainerConfiguratorImpl()
    var presenter: ExchangeContainerPresenter!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        configurator.configure(with: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialState()
        presenter.viewDidReady()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.prepare(segue)
    }
    
    func setupInitialState() {
        let rightButtonItem = UIBarButtonItem.init(title: "Exchange",
                                                   style: .done,
                                                   target: self,
                                                   action: #selector(ExchangeContainerViewController.exchangeAction))
        
        self.navigationItem.rightBarButtonItem = rightButtonItem

    }
    
    @objc func exchangeAction() {
        presenter.didPerformExchange()
    }
    
    func updateRateLabel(rate: String) {
        title = rate
    }
}
