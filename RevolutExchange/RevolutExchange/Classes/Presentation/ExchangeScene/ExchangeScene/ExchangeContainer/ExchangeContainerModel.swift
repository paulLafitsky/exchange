//
//  ExchangeModel.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/5/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation


typealias EstimatedValues = (toAccount: Account, fromAccount: Account)

protocol ExchangeContainerModel {
    var requestManager: APIRequestManager { get }
    var currencyManager: CurrencyManager { get }
    func clearTransaction()
    func collectCurrencyRates(succes: @escaping CompleationBlock, failure: @escaping ErrorCompleationBlock)
    func estimateAndStoreTransaction(transaction: Transaction) -> Double?
    func estimateAndUpdateStoredTransaction(forCurrency currency: Currency) -> Double?
    func provideAssets() -> Assets
    func exchange() -> Bool
    func provideFriendlyRate(fromCurrency: Currency, toCurrency: Currency) -> String
}

class ExchangeContainerModelImpl: ExchangeContainerModel {
    
    var requestManager: APIRequestManager
    var currencyManager: CurrencyManager
    var userManager = UserManager.defaultUserManager
    var currencyRates: [CurrencyRate] = [CurrencyRate]()
    private var transaction: Transaction?
    
    init(requestManager: APIRequestManager, currencyManager: CurrencyManager) {
        self.requestManager = requestManager
        self.currencyManager = currencyManager
    }

    func provideAssets() -> Assets {
        return userManager.user.assets
    }
    
    func collectCurrencyRates(succes: @escaping CompleationBlock, failure: @escaping ErrorCompleationBlock) {
        requestManager.getExchangesCurrenciesRate(succes: {[weak self] (rates) in
            self?.currencyRates.removeAll()
            self?.currencyRates.append(contentsOf: rates)
            succes()
        }, failure: failure)
    }
    
    func estimateAndUpdateStoredTransaction(forCurrency currency: Currency) -> Double? {
        guard var transaction = transaction else {
            return nil
        }
        transaction.to = currency
        return userManager.estimate(transaction:transaction, rate: rateForTransaction(fromCurrency: transaction.from, toCurrency: transaction.to))
    }
    
    func exchange() -> Bool {
        guard let transaction = transaction else { return false }
        if transaction.from == transaction.to {return false}
        return userManager.exchange(transaction: transaction, rate: rateForTransaction(fromCurrency: transaction.from, toCurrency: transaction.to))
    }
    
    func provideFriendlyRate(fromCurrency: Currency, toCurrency: Currency) -> String {
        let friendlyTransaction = Transaction(from: fromCurrency, to: toCurrency, amount: 1)
        let estimatedRate = userManager.estimate(transaction: friendlyTransaction, rate: rateForTransaction(fromCurrency: fromCurrency, toCurrency: toCurrency))
        return "\(fromCurrency.symbol) 1 = \(toCurrency.symbol) \(estimatedRate)"
    }
    
    func estimateAndStoreTransaction(transaction: Transaction) -> Double? {
        self.transaction = transaction
        return userManager.estimate(transaction:transaction, rate: rateForTransaction(fromCurrency: transaction.from, toCurrency: transaction.to))
    }
    
    func clearTransaction() {
        self.transaction = nil
    }
    
    //MARK: Private
    
    func rateForTransaction(fromCurrency: Currency, toCurrency: Currency) -> Double {
        let fromCurrencyRate = findRate(currency: fromCurrency)
        let toCurrencyRate = findRate(currency: toCurrency)
        if (fromCurrency == .eur) {
            return toCurrencyRate
        }
        return toCurrencyRate / fromCurrencyRate
    }
    
    private func findRate(currency: Currency) -> Double {
        if (currency == .eur) {return 1}
        guard let rate = currencyRates.first(where: {$0.code == currency.rawValue}) else {
            return 0
        }
        guard let rateValue = Double(rate.value) else {
            return 0
        }
        return rateValue
    }
    
}
