//
//  ExchangePresenter.swift
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/5/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import UIKit

protocol ExchangeContainerPresenter {
    var view: ExchangeContainerViewController? {get}
    var model: ExchangeContainerModel {get}
    func prepare(_ segue: UIStoryboardSegue)
    func viewDidReady()
    func didPerformExchange()
}

class ExchangeContainerPresenterImpl: ExchangeContainerPresenter, AssetsPageDelegate {
    
    var currencyRates: [CurrencyRate] = [CurrencyRate]()
    var fromCurrencyPresenter : AssetsPagePresenterPublic!
    var toCurrencyPresenter : AssetsPagePresenterPublic!
    
    struct ExchangeSegueIdentifier {
        static let fromIdentifier = "Segue.Exchange.From.Ident"
        static let toIdentifier = "Segue.Exchange.To.Ident"
    }
    
    weak var view: ExchangeContainerViewController?
    var model: ExchangeContainerModel
    var timer: Timer?
    
    init(_ view: ExchangeContainerViewController, model: ExchangeContainerModel) {
        self.view = view
        self.model = model
    }
    
    func viewDidReady() {
        startCollectingData()
        
    }
    
    func prepare(_ segue: UIStoryboardSegue) {
        guard let identifier = segue.identifier else {
            return
        }
        guard let concretePageViewController = segue.destination as? AssetsPageViewController else {
            return
        }
        switch identifier {
        case ExchangeSegueIdentifier.fromIdentifier:
            fromCurrencyPresenter = configurePageViewScene(controller: concretePageViewController, mode: .from)
            break
        case ExchangeSegueIdentifier.toIdentifier:
            toCurrencyPresenter = configurePageViewScene(controller: concretePageViewController, mode: .to)
            break
        default:
            break
        }
    }
    
    func didPerformExchange() {
        if model.exchange() {
            fromCurrencyPresenter.reset()
            toCurrencyPresenter.reset()
            return
        }
        //TODO: Throw error
    }
    
    //MARK: - AssetsPageDelegate
    
    func assetsDidTriggeredAmountChanged(amount: Double, currency: Currency, mode: ExchangeMode) {
        switch mode {
        case .from:
            let transaction = Transaction(from: currency,
                                          to: toCurrencyPresenter.getActiveCurrency(),
                                          amount: amount)
            updateEsimation(transaction: transaction, currencyPresenter: toCurrencyPresenter)
            break
        case .to:
            
            let transaction = Transaction(from: fromCurrencyPresenter.getActiveCurrency(),
                                          to: currency,
                                          amount: amount)
            updateEsimation(transaction: transaction, currencyPresenter: fromCurrencyPresenter)
            break
        }
    }
    
    func assetsDidActiveCurrencyChanged(currency: Currency, mode: ExchangeMode) {
        updateFriendlyRate()
        if toCurrencyPresenter.isActive() {
            guard let futureAmount = self.model.estimateAndUpdateStoredTransaction(forCurrency: currency) else {
                fromCurrencyPresenter.showFutureBalance(currency: currency, balance: 0)
                return
            }
            fromCurrencyPresenter.showFutureBalance(currency: currency, balance: futureAmount)
            return
        } else if fromCurrencyPresenter.isActive() {
            guard let futureAmount = self.model.estimateAndUpdateStoredTransaction(forCurrency: currency) else {
                toCurrencyPresenter.showFutureBalance(currency: currency, balance: 0)
                return
            }
            toCurrencyPresenter.showFutureBalance(currency: currency, balance: futureAmount)
            return
        }
        model.clearTransaction()
        fromCurrencyPresenter.reset()
        toCurrencyPresenter.reset()
    }
        
    //MARK: - Private
    
    func updateEsimation(transaction: Transaction, currencyPresenter: AssetsPagePresenterPublic) {
        let futureAmount = model.estimateAndStoreTransaction(transaction: transaction)
        currencyPresenter.showFutureBalance(currency: currencyPresenter.getActiveCurrency(), balance: futureAmount!)
    }
    
    private func configurePageViewScene(controller: AssetsPageViewController, mode: ExchangeMode) -> AssetsPagePresenterPublic {
        let currencyModel = AssetsPageModelImpl(assets: model.provideAssets(), mode: mode)
        let presenter = AssetsPagePresenterImpl(model: currencyModel, view: controller)
        presenter.delegate = self
        controller.presenter = presenter
        return presenter
    }
    
    func startCollectingData() {
        timer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true, block: { [weak self] (_) in
            if let strongSelf = self {
                strongSelf.obtainRates()
            }
        })
        timer?.fire()
    }
    
    func updateFriendlyRate() {
        let friendlyRate = self.model.provideFriendlyRate(fromCurrency: fromCurrencyPresenter.getActiveCurrency(), toCurrency: toCurrencyPresenter.getActiveCurrency())
        view?.updateRateLabel(rate: friendlyRate)
    }
    
    func obtainRates() {
        model.collectCurrencyRates(succes: {
            self.updateFriendlyRate()
        }, failure: { (error) in
            //TODO error
        })
    }
}
