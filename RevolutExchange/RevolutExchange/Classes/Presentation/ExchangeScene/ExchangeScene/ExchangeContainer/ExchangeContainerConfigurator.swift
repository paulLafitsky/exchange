//
//  ExchangeContainerConfigurator.h
//  RevolutExchange
//
//  Created by Poul Lafitskiy on 10/8/17.
//  Copyright © 2017 Paul Lafytskyi. All rights reserved.
//

import Foundation
import Moya

protocol ExchangeContainerConfigurator {
    func configure(with view: ExchangeContainerViewController)
}

class ExchangeContainerConfiguratorImpl: ExchangeContainerConfigurator {
    
    init() {}
    
    func configure(with view: ExchangeContainerViewController) {
        let service = APIServiceImplementation(apiProvider: MoyaProvider())
        let apiRequestManager = APIRequestManagerImpl(apiService: service)
        let exchangeModel = ExchangeContainerModelImpl(requestManager: apiRequestManager, currencyManager: CurrencyManager())
        let exchangePresenter = ExchangeContainerPresenterImpl.init(view, model: exchangeModel)
        view.presenter = exchangePresenter
    }
}
